var menuState = {
    
    preload: function() {
        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        // Loat game sprites.
        game.load.image('background', 'assets/background_menu.jpg');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet1','assets/bullet.png');
        game.load.image('star','assets/star.png');
        game.load.image('black','assets/Black.jpg');



    },
    create: function() {
        game.stage.backgroundColor = '#000000';
        var delay = 0;
        for (var i = 0; i < 300; i++)
        {
          var sprite = game.add.sprite( game.world.randomX, + game.world.randomY, 'star');

          sprite.scale.set(game.rnd.realInRange(0.1,0.6));
          sprite.alpha = 0;

          var speed = game.rnd.between(400, 600);

          game.add.tween(sprite).to({ alpha: 1 }, speed, Phaser.Easing.Sinusoidal.InOut, true, delay, 200, false).yoyo(true,100);

          delay += 20;
         }
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        var graphics = game.add.graphics(100, 100);
        graphics.lineStyle(2, 0xFFFFFF, 1);
        graphics.drawRect(162, 228+120, 127, 40);
        this.graphics_2 = game.add.graphics(100,100);
        this.graphics_2.beginFill(0xCCCCCC, 0.2);
        this.graphics_2.drawRect(112, 228+50, 227, 40);
        this.graphics_2.endFill();
        this.color = 0xCCCCCC;
        this.start = game.add.text(game.width/2, game.height/2+120, 'START', { font: '32px Arial', fill: '#ffffff',});
        this.start.inputEnabled = true;
        this.start.anchor.setTo(0.5,0.5);
        this.start.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);

        this.one = game.add.text(game.width/2, game.height/2+170, 'One Player', { font: '32px Arial', fill: '#ffffff',});
        this.one.inputEnabled = true;
        this.one.anchor.setTo(0.5,0.5);
        this.one.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.one.alpha = 0;

        this.two = game.add.text(game.width/2, game.height/2+220, 'Two Players', { font: '32px Arial', fill: '#ffffff',});
        this.two.inputEnabled = true;
        this.two.anchor.setTo(0.5,0.5);
        this.two.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);
        this.two.alpha = 0;

        this.raiden = game.add.text(game.width/2, -100, "Raiden", { font: "100px Arial Black", fill: "#C0C0CC" });
        this.raiden.stroke = "#555555";
        this.raiden.strokeThickness = 8;
        this.raiden.setShadow(2, 2, "#333333", 2, true, false);
        this.raiden.anchor.setTo(0.5);
        game.add.tween(this.raiden).to({y: game.height/2-150},1500, Phaser.Easing.Bounce.Out, true, 1000 ,0);
        
        window.graphics = graphics;

    //  This is our BitmapData onto which we'll draw the word being entered
         bmd = game.make.bitmapData(game.width, game.height);
         bmd.context.font = '32px Arial';
         bmd.context.fillStyle = '#ffffff';
         bmd.context.textAlign = "center";
         bmd.addToWorld();
         bmd.context.fillText(username,game.width/2, 408);
         
         

         this.key_backspace = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
         this.key_backspace.onDown.add(this.back_username, this);

    //  Capture all key presses
         game.input.keyboard.addCallbacks(this, null, null, this.keyPress);

         this.black = game.add.sprite(0,0,'black');
         this.black.alpha = 0;


    },
    update: function() {
        this.graphics_2.clear();
        this.graphics_2 = game.add.graphics(100,100);
        this.graphics_2.beginFill(this.color, 0.2);
        this.graphics_2.drawRect(112, 228+50, 227, 40);
        this.graphics_2.endFill();
        this.start.events.onInputOver.add(()=>{
            this.start.backgroundColor = 'rgb(255,255,255)';
            this.start.fill = 'rgb(150,150,150)';
            graphics.lineStyle(2, 0x969696, 1);
            graphics.drawRect(162, 228+120, 127, 40);
        }, this);
        this.start.events.onInputOut.add(()=>{
            this.start.backgroundColor = 'rgb(150,150,150)';
            this.start.fill = 'rgb(255,255,255)';
            graphics.lineStyle(2, 0xFFFFFF, 1);
            graphics.drawRect(162, 228+120, 127, 40);
        }, this);
        this.start.events.onInputDown.add(()=>{
                
                game.add.tween(this.one).to({alpha:1},1000, Phaser.Easing.Linear.None, true, 500 ,0).onComplete.add(()=>{
                    game.add.tween(this.two).to({alpha:1},1000, Phaser.Easing.Linear.None, true,  0,0);
                },this);
                showplayer_flag = 1;
        },this);

        this.one.events.onInputDown.add(()=>{
            if(username!="" && username!="請輸入ID"){

                if(showplayer_flag == 1){
                    game.add.tween(this.black).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 1000 ,0).onComplete.add(()=>{
                game.state.start('play');
                player_number = 1;
                },this);
                showplayer_flag = 0;
                }              
            }           
        },this);
        this.two.events.onInputDown.add(()=>{
            if(username!="" && username!="請輸入ID"){

                if(showplayer_flag == 1){
                    game.add.tween(this.black).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 1000 ,0).onComplete.add(()=>{
                game.state.start('play');
                player_number = 2;
                },this);
                showplayer_flag = 0;
                }              
            }           
        },this);
        this.black = game.add.sprite(0,0,'black');
        this.black.alpha = 0;

    },
    back_username:function(){
      var new_username="";
      for(var i = 0;i<username.length-1;i++){
          new_username+=username[i];
      }
      username = new_username;
      bmd.cls();
      bmd.context.fillText(username,game.width/2, 408); 
    },
    create_alert:function (type, message) {
        var alertarea = document.getElementById('custom-alert');
        if (type == "success") {
            str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        } else if (type == "error") {
            str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
        }
    },
    keyPress:function (char) {
        bmd.cls();
        if(username == "請輸入ID")username = "";
        if(username.length<=9)username+=char;
        bmd.context.fillText(username,game.width/2, 408);   
    }
    
    
};
var username = "請輸入ID";
var bmd;
var showplayer_flag = 0;
var player_number = 1;





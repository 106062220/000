var endState = {
    
    preload: function() {
        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');
        // Loat game sprites.
        game.load.image('background', 'assets/background_menu.jpg');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet1','assets/bullet.png');
        game.load.image('star','assets/star.png');
        game.load.image('black','assets/Black.jpg');
    },
    create: function() {
        game.stage.backgroundColor = '#000000';
        var delay = 0;
        for (var i = 0; i < 300; i++)
        {
          var sprite = game.add.sprite( game.world.randomX, + game.world.randomY, 'star');

          sprite.scale.set(game.rnd.realInRange(0.1,0.6));
          sprite.alpha = 0;

          var speed = game.rnd.between(400, 600);

          game.add.tween(sprite).to({ alpha: 1 }, speed, Phaser.Easing.Sinusoidal.InOut, true, delay, 200, false).yoyo(true,100);

          delay += 20;
         }
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();
        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        this.graphics_1 = game.add.graphics(100, 100);
        this.graphics_2 = game.add.graphics(100, 100);

        this.graphics_1.lineStyle(2, 0xFFFFFF, 1);
        this.graphics_1.drawRect(142, 228+210, 167, 40);
        this.restart = game.add.text(game.width/2, game.height/2+210, 'RESTART', { font: '32px Arial', fill: '#ffffff',});
        this.restart.inputEnabled = true;
        this.restart.anchor.setTo(0.5,0.5);
        this.restart.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);

        this.graphics_2.lineStyle(2, 0xFFFFFF, 1);
        this.graphics_2.drawRect(142, 228+210+70, 167, 40);
        this.quit = game.add.text(game.width/2, game.height/2+210+70, 'QUIT', { font: '32px Arial', fill: '#ffffff',});
        this.quit.inputEnabled = true;
        this.quit.anchor.setTo(0.5,0.5);
        this.quit.setShadow(3, 3, 'rgba(0,0,0,0.5)', 5);

        this.raiden = game.add.text(game.width/2, game.height/2-230, "Score", { font: "100px Arial Black", fill: "#C0C0CC" });
        this.raiden.stroke = "#555555";
        this.raiden.strokeThickness = 8;
        this.raiden.setShadow(2, 2, "#333333", 2, true, false);
        this.raiden.anchor.setTo(0.5);
        this.raiden.scale.setTo(0,0);
        game.add.tween(this.raiden.scale).to({x:1,y:1},1500, Phaser.Easing.Bounce.Out, true, 1000 ,0);


        this.scoreLabel = game.add.text(game.width/2, game.height/2-130, '0', { font: '72px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5,0.5);
        this.score_flag = game.add.text(game.width/2, game.height/2-30, '0', { font: '72px Arial', fill: '#ffffff' });
        this.score_flag.anchor.setTo(0.5,0.5);
        this.score_flag.text = 0;
        this.score_flag.alpha = 0;
        game.add.tween(this.score_flag).to({text:score},10000, Phaser.Easing.Cubic.Out, true, 1000 ,0);

        this.rank = game.add.text(game.width/2, game.height/2-50, "Rank", { font: "32px Arial Black", fill: "#C0C000" });
        this.rank.stroke = "#555555";
        this.rank.strokeThickness = 8;
        this.rank.setShadow(2, 2, "#333333", 2, true, false);
        this.rank.anchor.setTo(0.5);

        this.black = game.add.sprite(0,0,'black');
        this.black.alpha = 0;
        //firebase.database().ref('rank').orderByChild('score');
        //console.log(rank_array[0]);
        this.show_rank();
        this.black = game.add.sprite(0,0,'black');
        this.black.alpha = 0;

    },
    show_rank: function(){
        var rank_array =[];
        const prossess = async() =>{
                     await firebase.database().ref('rank').once('value')
                     .then((child)=>{
                        for(var j in child.val()){
                              rank_array.push([child.val()[j].score,j]);
                         }
                           rank_array.sort(function(x,y){return y[0]-x[0];});
                        }      
                                          
                         );
                         var color;
                         for(var i=0;i<5;i++){
                            if(i==0)color = '#DAB273';
                            else if(i==1)color = '#E9E9D8';
                            else if(i==2)color = '#BA6E40';
                            else color = '#444444';
                            var a = game.add.text(game.width/2-100, game.height/2+i*30, '0', { font: '20px Arial', fill: color });
                            a.text = (i+1).toString()
                            a.anchor.setTo(0.5,0.5);
                            a.scale.setTo(0,0);
                            game.add.tween(a.scale).to({x:1,y:1},200, Phaser.Easing.Linear.None, true, 500+i*300 ,0);
                         }
                         for(var i=0;i<5;i++){
                            if(i==0)color = '#DAB273';
                            else if(i==1)color = '#E9E9D8';
                            else if(i==2)color = '#BA6E40';
                            else color = '#444444';
                            var a = game.add.text(game.width/2, game.height/2+i*30, '0', { font: '20px Arial', fill: color });
                            a.text = rank_array[i][1];
                            a.anchor.setTo(0.5,0.5);
                            a.scale.setTo(0,0);
                            game.add.tween(a.scale).to({x:1,y:1},200, Phaser.Easing.Linear.None, true, 500+i*300 ,0);
                         }
                         for(var i=0;i<5;i++){
                            if(i==0)color = '#DAB273';
                            else if(i==1)color = '#E9E9D8';
                            else if(i==2)color = '#BA6E40';
                            else color = '#444444';
                            var a = game.add.text(game.width/2+100, game.height/2+i*30, '0', { font: '20px Arial', fill: color });
                            a.text =rank_array[i][0];
                            a.anchor.setTo(0.5,0.5);
                            a.scale.setTo(0,0);
                            game.add.tween(a.scale).to({x:1,y:1},200, Phaser.Easing.Linear.None, true, 500+i*300 ,0);
                         }
        }
        prossess();
    },
    update: function() {
        this.restart.events.onInputOver.add(()=>{
            this.restart.backgroundColor = 'rgb(255,255,255)';
            this.restart.fill = 'rgb(150,150,150)';
            this.graphics_1.lineStyle(2, 0x969696, 1);
            this.graphics_1.drawRect(142, 228+210, 167, 40);
        }, this);
        this.restart.events.onInputOut.add(()=>{
            this.restart.backgroundColor = 'rgb(150,150,150)';
            this.restart.fill = 'rgb(255,255,255)';
            this.graphics_1.lineStyle(2, 0xFFFFFF, 1);
            this.graphics_1.drawRect(142, 228+210, 167, 40);
        }, this);
        this.restart.events.onInputDown.add(()=>{
            game.add.tween(this.black).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 500 ,0).onComplete.add(()=>{
                game.state.start('play');
                bigger_flag = true;
                level = 0;
                score = 0;
                boss1_mode = 0;
                score_flag = 0;
                canbeattackTime = 0;
                bullet_type = 1;
            },this);
            
        },this);
        this.quit.events.onInputOver.add(()=>{
            this.quit.backgroundColor = 'rgb(255,255,255)';
            this.quit.fill = 'rgb(150,150,150)';
            this.graphics_2.lineStyle(2, 0x969696, 1);
            this.graphics_2.drawRect(142, 228+210+70, 167, 40);
        }, this);
        this.quit.events.onInputOut.add(()=>{
            this.quit.backgroundColor = 'rgb(150,150,150)';
            this.quit.fill = 'rgb(255,255,255)';
            this.graphics_2.lineStyle(2, 0xFFFFFF, 1);
            this.graphics_2.drawRect(142, 228+210+70, 167, 40);
        }, this);
        this.quit.events.onInputDown.add(()=>{
            game.add.tween(this.black).to({alpha:1},1500, Phaser.Easing.Linear.None, true, 500 ,0).onComplete.add(()=>{
                game.state.start('menu');
                bigger_flag = true;
                username = "請輸入ID";
                level = 0;
                score = 0;
                boss1_mode = 0;
                score_flag = 0;
                canbeattackTime = 0;
                bullet_type = 1;
                
            },this)
            
        },this);
        this.scoreLabel.text = game.math.roundAwayFromZero(this.score_flag.text);
        if(this.scoreLabel.text == score && bigger_flag){
            game.add.tween(this.scoreLabel.scale).to({x:1.2,y:1.2},300, Phaser.Easing.Linear.Out, true, 1000 ,0).yoyo(100,true);
            bigger_flag=false;
        }
    },
    
    
};
game.state.add('play', playState);
game.state.add('menu', menuState);
game.state.add('end', endState);
game.state.start('menu');
var score_flag=0;
var bigger_flag = true;




